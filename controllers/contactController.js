const asyncHandler=require("express-async-handler");
const  Contacts=require("../models/contactModel");
//@des: Get Contacts
// @route:get/api/contacts
// @access:Private
const getContacts=asyncHandler(async(req,res)=>{
       const contact= await Contacts.find({user_id:req.user.id});
        res.status(200).json({contact});
});

//@des: create Contacts
// @route:post/api/contact
// @access:Private
const createContact=asyncHandler(async(req,res)=>{
    console.log("The request contact are:",req.body);
    const { name,email,phone}=req.body;
    if(!name || !email || !phone){
        // res.json({Message:"filed is empty,please fill the filed"});
        res.status(400);
       res.json ("all fild are mandatory !");  //must be write to dispaly the error message
    }
     const contact= await Contacts.create({
        name,
        email,
        phone,
        user_id:req.user.id
     })
    
    res.status(201).json({Message:"create all contacts"});
});

//@des: Get Contact
// @route:get/api/contact/:id
// @access:Private
const getContact=asyncHandler(async(req,res)=>{
    const contact=await Contacts.findById(req.params.id);
    if(!contact){
        res.status(500);
       res.json(" contact not found");
      
        
    }
    // res.status(200).json({Message:`Get contact ${req.params.id}`});
    res.status(200).json({contact});
});

//@des: update Contacts
// @route:update/api/contact
// @access:Private
const updateContact=asyncHandler(async(req,res)=>{
    const contact= await Contacts.findById(req.params.id);
    if(!contact){
       res.json("updated contact notfound");
    }
    if(Contacts.user_id.toString() !== req.user.id){
        res.status(403);
        res.json("User don't have a permission to Update other user Contact");
    }
    const contactUpdated=await Contacts.findByIdAndUpdate(
        req.params.id,
        req.body,
        {new :true}
    )
    res.status(200).json(contactUpdated);
});

//@des: delete Contacts
// @route:delete/api/contact
// @access:Private
const deleteContact=asyncHandler(async(req,res)=>{
    const contact= await Contacts.findById(req.params.id);
    if(!contact){
        res.status(404);
        res.json("no contact to be deleted");
    }

    if(Contacts.user_id.toString() !== req.user.id){
        res.status(403);
        res.json("User don't have a permission to Delete other user Contact");
    }
     await Contacts.deleteOne({_id:req.params.id});
     res.status(200).json(contact);
     
});

module.exports={
                 createContact,
                 getContacts,
                 getContact,
                 updateContact,
                 deleteContact
                }