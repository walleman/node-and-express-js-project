const asyncHandler=require("express-async-handler");
const Users=require("../models/userModel");
const bcrypt=require("bcrypt");
const jwt=require("jsonwebtoken")
// Desc :register users;
// Route: api/user/register
// access public
const registeUser=asyncHandler (async(req,res)=>{
    const {username,email,password}=req.body;
    if( !username || !email || !password){
        res.status(400);
         res.json("fileds are required please fill the fileds correctly");
    }
    const avilableUser=await Users.findOne({email});
    if(avilableUser){
        res.status(400);
        res.json("email is allredy taken");
    }
    // hasing password
    const hashPassword=await bcrypt.hash(password,10);
    // console.log("hashed password",hashPassword);
    const user= await Users.create({
        username,
        email,
        password:hashPassword
    })
    res.status(200);
    res.json(user);
  });
// Desc :register users;
// Route: api/user/register
// access public
const loginUser=asyncHandler (async(req,res)=>{
    const {email,password}=req.body;
    if(!email || !password){
        res.status(400);
        res.json("All filed are mandatory");
    }
    // check if the user is register or not based on email;
    const user = await Users.findOne({email});
    // compare bcrypt password and  new request password, and check the user is in database or registered;
    if(user && bcrypt.compare(password,user.password)){
        const accessToken=  jwt.sign({
             user:{ 
              username:user.username,
              email:user.email,
              password:user.password,
             },
        },process.env.ACCESS_TOKEN_SECRET,{expiresIn:"15m"})
        res.json({accessToken});
    }
    else{
        res.status(401);
        res.json("User not Authenticated");
    }

    res.json({"message":"users login successfully"});
});
// Desc :Current users;
// Route: api/user/Current
// access private
const currentUser=asyncHandler (async(req,res)=>{
    res.json(req.user);
});
module.exports={registeUser,loginUser,currentUser}