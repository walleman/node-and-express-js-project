const mongoose= require("mongoose");

const contactSchema=mongoose.Schema({
    user_id: {
        type:mongoose.Schema.Types.ObjectId,
        require:true,
        ref:"User"
     },
    name:{
       type:String,
       require:[true,"please fill the name filed"]

    },     
    email:{
        type:String,
        require:[true,"please fill the email filed"]
    },
    phone:{
        type:String,
        require:[true,"please fill the phone filed"]
    },
},
{
    timestamps:true,
}
)

module.exports=mongoose.model("contact",contactSchema)