const mongoose =require("mongoose");

const userSchema=mongoose.Schema({
 
     username:{
        type:String,
        require:[true,"please enter username"]
     },
    email: {
        type:String,
        require:[true,"please enter email address"],
        unique:[true,"email address are allready taken"]
    },
    password:{
        type:String,
        require:[true,"please ener password number"]
    }
},
{
    timestamps:true
}
)
module.exports=mongoose.model("user",userSchema)
