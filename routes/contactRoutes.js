const express=require('express');
const router=express.Router();

const {createContact,
    getContacts,
    getContact,
    updateContact,
    deleteContact}=require("../controllers/contactController");
const validateToken = require('../middleware/validateTokenHandler');

// if they have the same route,the we can place in the same line like this

router.use(validateToken);
router.route("/").get(getContacts).post(createContact);
router.route("/:id").get(getContact).put(updateContact).delete(deleteContact)

// // get all contacts;
// router.route("/").get(getContacts)

// // create  contact;
// router.route("/").post(createContact)

// // if they have the same route,the we can place in the same line like this
// // router.route("/:id").get(getContact).put(updateContact).delete(deleteContact)


// // get contact;
// router.route("/:id").get(getContact)

// // update contact;
// router.route("/:id").put(updateContact)

// // Delete Contact;
// router.route("/:id").delete(deleteContact)



module.exports=router;