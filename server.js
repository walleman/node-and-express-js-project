const express =require("express");
const errorHandler = require("./middleware/errorHandler");
const connectDB = require("./config/databaseConnection");
const dotenv=require('dotenv').config();

const app=express();
connectDB();
const port=process.env.PORT || 5000;
app.use(express.json());
app.use("/api/contacts",require("./Routes/contactRoutes"));
app.use("/api/users",require("./routes/userRoutes"));
// app.use(errorHandler);

app.listen(port,()=>{
    console.log(`Listenning.....${port}`);
});